 # JavaFx-2048 #

Ce README fait un petit résumé de ce qu'on a pu réaliser sur le projet GLI-JavaFx-2048.

### But de l'Application ###

* Conception d'un IHM Client Lourd en JAVAFX pour l'Application de Jeu 2048.

### Implémentation ###

* L'IHM a été implementé suivant le pattern MVC comme nous le montre l'architecture logicielle [ici](https://bitbucket.org/mamadian/tp-gli-2048/src/8d76430d1dec0f72633b900ef2e02d733e8913b5/Architecture%20Technique-JavaFx-2048%281%29.pdf?fileviewer=file-view-default).

### Choix de Conception ###

* Non utilisation du Fxml (trop verbeux)

* Utilisation du développement classique en passant directement par des interfaces et des classes Java

### Execution ###

* mvn clean install
* Run src/main/java/sample/main.java