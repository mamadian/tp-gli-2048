package model;

/**
 * Created by plouzeau on 2014-10-09.
 */
public class TileImpl implements Tile  {

    private  int rank;

    public TileImpl(int rank) {
        this.rank = rank;
    }

   
    public int getRank() {
        return this.rank;
    }

   
    public void incrementRank() {
        this.rank += this.rank;
    }
}
