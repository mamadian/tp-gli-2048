package ihm;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderImage;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import model.Tile;

public class TileUI extends Label{
	private String value;

    private Tile labelTile;
   

    public TileUI(Tile tile) {
        this.labelTile = tile;

        final int squareSize = BoardUI.CELL_SIZE - 20;
        setMinSize(squareSize, squareSize);
        setMaxSize(squareSize, squareSize);
        setPrefSize(squareSize, squareSize);
        setAlignment(Pos.CENTER);
        if (labelTile != null) {
            this.value = "" + labelTile.getRank();
        }
        setStyle("-fx-background-color: linear-gradient(to bottom, #f2f2f2, #d4d4d4);"
                + " -fx-border: 12px solid; -fx-border-color: white; -fx-background-radius: 15.0;"
                + " -fx-border-radius: 15.0;");
       
        setText(this.value);
    }
}
