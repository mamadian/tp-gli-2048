package ihm;

import java.util.Observable;
import java.util.Observer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.Board;
import model.Tile;
import sample.Controller;

public class BoardUI extends Application implements Observer{
	
	public static final int CELL_SIZE = 128;

    private static final String FONT_NAME = "Arial";
    String scoreValue = "Score: 0";
    private Controller controller;
    private Tile[][] Tabtile;
    private GridPane grid;
    private boolean isStart = false;
	@Override
	 public void start(Stage primaryStage) throws Exception{
		//creation d'un nouveau group
		Group root = new Group();
		//creation d'une scene avec des taille 500x600
        Scene scene = new Scene(root, 480, 480);
        //mettre un titre
        primaryStage.setTitle("2048");
        // empecher de le redimensionnement
        primaryStage.setResizable(false);
      //initialistion du controller
        this.controller = new Controller(this);
        // creation d'un grid et le positionner au centre de mon primaryStage
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setStyle("-fx-background-color: #336655;");
        grid.setGridLinesVisible(true);
        
        
        //ajouter un evenement sur la scene quand on appuie sur les touches de direction
        scene.setOnKeyReleased(new EventHandler<KeyEvent>(){
			public void handle(KeyEvent event) {
				switch (event.getCode().toString()){
				case "RIGHT": controller.right(); System.out.println("DROITE");
					break;
				case "LEFT":  controller.left(); System.out.println("GAUCHE");
					break;
				case "UP": controller.up(); System.out.println("HAUT");
					break;
				case "DOWN": controller.down();System.out.println("BAS");
					break;
				}
			}
        });
        // ajouter a mon group ma grid
        root.getChildren().add(grid);
        grid.setPadding(new Insets(10));
        
        final HBox hbox = new HBox();
        hbox.setPadding(new Insets(10, 10, 10, 10));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #336699;");
        final Button startButton = new Button("Start");
        startButton.setPrefSize(100, 20);
        Label score = new Label(scoreValue);
        score.setPadding(new Insets(5,5,5,25));
        score.setStyle("-fx-text-fill: white; -fx-font-size: 12pt;");
		startButton.setOnAction(new EventHandler<ActionEvent>() {
		            
		            public void handle(ActionEvent event) {
		            	isStart = !isStart;
		                if(isStart){
		                	
		                	hbox.setVisible(false);
		                }else {
		                    startButton.setText("Start");
		                }
		            	controller.up();
		            }
		 });
        
        hbox.setSpacing(8);
        hbox.getChildren().add(startButton);
        root.getChildren().add(score);
        root.getChildren().add(hbox);
        
        // ajouter ma scene a ma primaryStage
        primaryStage.setScene(scene);
        primaryStage.show();
	 }

	public void update(Observable o, Object object) {
		 if (o instanceof Board) {
			 	Tabtile = (Tile[][]) object;
	            grid.getChildren().clear();
	            grid.setAlignment(Pos.CENTER);

	            for (int i = 0; i < Controller.SIDE_SIZE_IN_SQAURE; i++) {
	                for (int j = 0; j < Controller.SIDE_SIZE_IN_SQAURE; j++) {
	                    Tile tile = Tabtile[i][j];
	                    int value = 0;
	                    if (tile != null) {
	                        value = tile.getRank();
	                    }
	                    final int size = value < 100 ? 36 : value < 1000 ? 32 : 24;
	                    final Font font = new Font(FONT_NAME, size);
	                    Label label = new TileUI(tile);
	                    label.setFont(font);
	                    grid.add(label, j, i);
	                }
	            }
	            grid.setPadding(new Insets(25, 25, 25, 25));
	            grid.setGridLinesVisible(true);
	            System.out.println("Tabtile \t" + Tabtile.toString());
	        }
	}	

}
