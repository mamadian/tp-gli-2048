package sample;

import java.util.Random;
import java.util.logging.Logger;

import ihm.BoardUI;
import model.Board;
import model.BoardImpl;

public class Controller{
	
	public static final int SIDE_SIZE_IN_SQAURE = 4;
	private BoardUI boardIU;
	private Board board;
	
	public Controller(BoardUI boardIU){
		this.boardIU=boardIU;
		board= new BoardImpl(SIDE_SIZE_IN_SQAURE);
		board.loadBoard(initMatrix());
		board.addObserver(this.boardIU);
	}

	/**
	 * Cette fonction permet d'initialiser la matrice avec soit 2 ou 4 dedans
	 * 
	 * @returnune matrice
	 */
	private int[][] initMatrix() {
		int[][] resultat= new int[SIDE_SIZE_IN_SQAURE][SIDE_SIZE_IN_SQAURE];
		int x1= new Random().nextInt(SIDE_SIZE_IN_SQAURE);
		int y1= new Random().nextInt(SIDE_SIZE_IN_SQAURE);
		
		int x2= new Random().nextInt(SIDE_SIZE_IN_SQAURE);
		int y2= new Random().nextInt(SIDE_SIZE_IN_SQAURE);
		
		resultat[x1][y1]=new Random().nextDouble() < 0.9 ? 2 : 4;
		resultat[x2][y2]=new Random().nextDouble() < 0.9 ? 2 : 4;
		
		return resultat;
		
	}
	/**
	 * cette methode permet d'effectuer un deplacement vers la droite
	 */
	public void right() {
        board.packIntoDirection(Board.Direction.RIGHT);
        board.commit();
        board.printBoard(Logger.getGlobal(), "Right");
    }

	/**
	 * cette  methode permet d'effectuer un deplacement vers la gauche
	 */
    public void left() {
        board.packIntoDirection(Board.Direction.LEFT);
        board.commit();
        board.printBoard(Logger.getGlobal(), "left");
    }

    /**
	 * cette  methode permet d'effectuer un deplacement vers le haut
	 */
    public void up() {
        board.packIntoDirection(Board.Direction.TOP);
        board.commit();
        board.printBoard(Logger.getGlobal(), "TOP");
    }
    /**
	 * cette  methode permet d'effectuer un deplacement vers le bas
	 */
    public void down() {
        board.packIntoDirection(Board.Direction.BOTTOM);
        board.commit();
        board.printBoard(Logger.getGlobal(), "down");
    }
}
